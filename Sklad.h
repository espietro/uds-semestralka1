#pragma once
#include "../structures/heap_monitor.h"
#include "Paleta.h"
#include "Kamion.h"
#include "Dodavatel.h"
#include "..\structures\list\array_list.h"

using namespace structures;
class Sklad
{
public:
	Sklad();
	~Sklad();
	bool load(ifstream & infile);
	bool save(ofstream & outfile);
	bool load_palety(ifstream& infile,Kamion& kam);
	bool pridaj_dodavatel(Dodavatel*);
	bool je_dodavatel(string nazov);
	void vymaz_dodavatelov();
	void vypis_dodavatelov();
	bool pridaj_kamion(Kamion * kamion);

	void vymaz_kamiony();
	
private:
	//ArrayList<Paleta*> palety_;
	ArrayList<Kamion*> ohlasene_kamiony_;
	LinkedList<Dodavatel*> dodavatelia_;
	
};

