#pragma once
#include <cstdlib>
#include <iostream>
#include "../structures/heap_monitor.h"
#include "../structures/stack/explicit_stack.h"
#include "Paleta.h"

using namespace structures;
class Kamion
{
public:
	Kamion();
	Kamion(size_t nosnost_, Date);
	~Kamion();
	bool operator==(const Kamion& other) const;
	Date get_datum();
	void vymaz_palety();
	bool pridaj_paletu(Paleta*);
	size_t size();
private:
	//size_t id_;
	size_t nosnost_;
	size_t obsadenost_;
	Date datum_;
	ExplicitStack<Paleta*> palety_;
};