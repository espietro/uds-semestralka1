#pragma once
#include <iostream>
#include <fstream>
#include "Dodavatel.h"
#include "../structures/heap_monitor.h"
#include "Date.h"


class Paleta
{
public:

	Paleta();
	Paleta(size_t region, size_t weight, Date date, bool je_prioritna = false);
	~Paleta();
	size_t get_vaha();


	bool operator==(const Paleta& other) const;
	string vypis();

	void save(ofstream& outfile);
	void load(ifstream& infile);


private:
	size_t region_;
	size_t vaha_;
	bool je_prioritna_;
	Date date_;	
	Dodavatel* dodavatel_;
};

