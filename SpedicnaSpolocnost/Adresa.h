#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "../structures/heap_monitor.h"
using namespace std;
class Adresa
{
public:

	Adresa();

	Adresa(string city_, string street_, int houseNo_) :
		city_(city_),
		street_(street_),
		houseNo_(houseNo_)
	{
	}

	~Adresa()
	{
	}

	string get_city() const
	{
		return city_;
	}
	string get_street() const
	{
		return street_;
	}
	int get_house_no() const
	{
		return houseNo_;
	}

	string vypis() const
	{
		string pom = city_ + ", " + street_ +" "+ std::to_string(houseNo_);
		return  pom;
	}

	bool operator==(Adresa& other) const
	{
		if (other.get_city() ==city_ && other.get_street() == street_ && other.get_house_no() == houseNo_)
		{
			return true;
		}
		return false;
	}

	void save(ofstream & outfile)
	{
		if (outfile.is_open())
		{
			outfile << city_  << "| " << street_ << "| " << houseNo_;
		}
		else
		{
			cout << "Nepodarilo sa ulozit adresu" << endl;
		}
	}

	void load(ifstream& infile)
	{
		if (infile.is_open())
		{
			getline(infile, city_, '|');
			//infile >> city_;
			getline(infile, street_, '|');
			//infile >> street_;
			infile >> houseNo_;
			
		}
		else
		{
			cout << "Nepodarilo sa nacitat adresu." << endl;
		}
	}
private:
	string city_;
	string street_;
	int houseNo_;
};

