#include "Vozidlo.h"
#include "../structures/heap_monitor.h"

Vozidlo::Vozidlo()
{
}

Vozidlo::Vozidlo(string ec, float nosnost,  Date datum_zarad, bool na_vyradenie = false, int opotrebenie = 0) :
	ec_(ec),
	nosnost_(nosnost),
	opotrebenie_(opotrebenie),
	na_vyradenie_(na_vyradenie),
	datum_zarad_(datum_zarad),
	palety_()
{
}

Vozidlo::~Vozidlo()
{
	vymaz_palety();
}

string Vozidlo::get_ec()
{
	return ec_;
}

float Vozidlo::get_nosnost()
{
	return nosnost_;
}

int Vozidlo::get_opotrebenie()
{
	return opotrebenie_;
}

bool Vozidlo::je_na_vyradenie()
{
	return na_vyradenie_;
}

Date Vozidlo::get_datum_zarad()
{
	return datum_zarad_;
}

string Vozidlo::vypis()
{
	string vrat;
	vrat = datum_zarad_.print_s() + "-> ec.: " + ec_ + ", nosnost: " + to_string(nosnost_);
	vrat += ", opotrebenie: " + to_string(opotrebenie_);
	return vrat;
}

void Vozidlo::vymaz_palety()
{
	for (size_t i = 0; i < palety_.size(); i++)
	{
		//palety_.pop(); 
		//potom budem pridavat palety len ako referencie zo skladu, takze v kufri bude mat len smerniky na palty v sklade -> tie nechceme zmazat (resp. az v spravnom case)
		delete palety_.pop();
	}
}

void Vozidlo::pridaj_paletu(Paleta * paleta)
{
	palety_.push(paleta);
}

void Vozidlo::save(ofstream & outfile)
{
	if (outfile.is_open())
	{
		outfile << ec_ << " " << nosnost_ << " " << (int)opotrebenie_ << " " << na_vyradenie_ << " ";
		datum_zarad_.save(outfile);
		int pocPaliet = palety_.size();
		outfile << "palety " << pocPaliet << " ";

		Array<Paleta*> pom(pocPaliet);

		for (int i = 0; i < pocPaliet; i++)
		{
			pom[i] = palety_.pop();	//ulozim stack do pola tak ze vrchol stacku je na indexe 0, v stacku su smerniky na objekty na halde takze tie nezanikaju, vymazavam len smerniky
		}
		for (int i = pocPaliet - 1; i >= 0; i--)
		{
			pom[i]->save(outfile);	//ulozim prvky pola do suboru tak aby bol posledny prvok na zaciatku, aby pri citani boli palety v stacku v spravnom poradi
		}
		for (int i = pocPaliet - 1; i >= 0; i--)
		{
			palety_.push(pom[i]);
			// delete pom[i]; toto nemozem zavolat lebo by som si vymazal prvok
		}
	}
	else
	{
		cout << "Nepodarilo sa ulozit Vozidlo" << endl;
	}
}

void Vozidlo::load(ifstream & infile)
{
	if (infile.is_open())
	{
		int opotreb;
		infile >> ec_;
		infile >> nosnost_;
		infile >> opotreb;// opotrebenie_;
		opotrebenie_ = (byte)opotreb;
		infile >> na_vyradenie_;
		datum_zarad_.load(infile);
		string pom;
		infile >> pom;
		if (pom == "palety")
		{
			size_t pocet_paliet;
			infile >> pocet_paliet;
			for (size_t i = 0; i < pocet_paliet; i++)
			{
				Paleta* p = new Paleta();
				p->load(infile);
				palety_.push(p);
			}
		}
		else
		{
			cout << "Nepodarilo sa nacitat palety" << endl;
		}
	} 
	else
	{
		cout << "Nepodarilo sa nacitat Vozidlo." << endl;
	}
}
