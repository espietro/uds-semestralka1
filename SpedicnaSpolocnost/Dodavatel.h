#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "../structures/heap_monitor.h"
#include "Adresa.h"
using namespace std;
class Dodavatel
{
public:
	Dodavatel();
	Dodavatel(string, Adresa);
	~Dodavatel();
	bool operator==(Dodavatel& other) const;
	string get_nazov() const;
	Adresa get_adresa() const;
	string vypis();
	bool save(ofstream& outfile);
	bool load(ifstream& infile);
private:
	string nazov_;
	Adresa adresa_;
};

