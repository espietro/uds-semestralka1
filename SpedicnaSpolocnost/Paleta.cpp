#include "Paleta.h"
#include "../structures/heap_monitor.h"
Paleta::Paleta()
{
}

Paleta::Paleta(size_t region, size_t weight, Date date, bool je_prioritna) :
	region_(region),
	vaha_(weight),
	date_(date),
	je_prioritna_(je_prioritna)
{
}

Paleta::~Paleta()
{
	dodavatel_ = nullptr;
}

size_t Paleta::get_vaha()
{
	return vaha_;
}

bool Paleta::operator==(const Paleta & other) const
{
	if (other.vaha_ == vaha_ && other.region_ == region_ && other.je_prioritna_ == je_prioritna_)
	{
		return true;
	}
	return false;
}

string Paleta::vypis()
{
	string s = "Region: " + to_string(region_) + ", vaha: " + to_string(vaha_ )+ ", prioritna: " + to_string(je_prioritna_);
	return s;
}

void Paleta::save(ofstream & outfile)
{
	if (outfile.is_open())
	{
		outfile << region_ << " " << vaha_ << " " << je_prioritna_ << " ";
		date_.save(outfile);
	}
	else
	{
		cout << "Nepodarilo sa ulozit Paleta";
	}
}

void Paleta::load(ifstream & infile)
{
	if (infile.is_open())
	{
		infile >> region_;
		infile >> vaha_;
		infile >> je_prioritna_;
		date_.load(infile);
	}
	else
	{
		cout << "Nepodarilo sa nacitat Paleta";
	}
}
