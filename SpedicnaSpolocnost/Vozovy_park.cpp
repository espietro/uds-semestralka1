#include "../structures/heap_monitor.h"
#include "Vozovy_park.h"


Vozovy_park::Vozovy_park()
{
}


Vozovy_park::~Vozovy_park()
{
	vymaz_auta();
}

/**
	Prida vozidlo do zoznamu podla jeho opotrebenia
	vozidlo - pointer na vozidlo ktore chceme pridat
*/
bool Vozovy_park::pridaj_vozidlo(Vozidlo* vozidlo)
{
	if (vozidla_.size() > 0)
	{
		for (size_t i = 0; i < vozidla_.size(); i++)
		{
			if (vozidlo->get_datum_zarad() <= vozidla_[i]->get_datum_zarad())
			{
				vozidla_.insert(vozidlo, i);
				return true;
			}
		}	
		vozidla_.add(vozidlo);
		return true;
	}
	else
	{
		vozidla_.add(vozidlo);
		return true;
	}
	
	return false;
}

void Vozovy_park::vypis_zoznam()
{
	if (vozidla_.isEmpty())
	{
		cout << "Zoznam je prazdny" << endl;
	}
	else
	{
		for (size_t i = 0; i < vozidla_.size(); i++)
		{
			cout << vozidla_[i]->vypis() << endl;
		}
	}
}

Vozidlo Vozovy_park::odstran_vozidlo(string ec)
{
	for (size_t i = 0; i < vozidla_.size(); i++)
	{
		if ((*vozidla_[i]).get_ec() == ec)
		{
			delete vozidla_[i];
			vozidla_.removeAt(i);
			break;
		}
	}
	return Vozidlo();
}

bool Vozovy_park::je_vozidlo(string ec)
{
	for (size_t i = 0; i < vozidla_.size(); i++)
	{
		if (vozidla_[i]->get_ec() == ec)
		{
			return true;
		}
	}
	return false;
}

void Vozovy_park::vymaz_auta()
{
	for (size_t i = vozidla_.size(); i > 0; i--)
	{
		delete vozidla_[i-1];
		vozidla_.removeAt(i-1);
	}
	
}

bool Vozovy_park::load(ifstream& infile)
{
	if (infile.is_open())
	{
		string pom;
		infile >> pom;
		if (pom == "vozidla")
		{
			size_t size;
			infile >> size;
			for (size_t i = 0; i < size; i++)
			{
				Vozidlo* newVoz = new Vozidlo();
				newVoz->load(infile);
				if (!je_vozidlo(newVoz->get_ec()))
				{
					pridaj_vozidlo(newVoz);
				}
				else
				{
					delete newVoz;
				}
			}
		}
		else
		{
			return false;
		}
		return true;
	}
	return false;
}

bool Vozovy_park::save(ofstream& outfile) {
	
	if (outfile.is_open())
	{		
		outfile << "vozidla ";
		outfile << vozidla_.size() << " ";
		for (size_t i = 0; i < vozidla_.size(); i++)
		{
			vozidla_[i]->save(outfile);
		}
		return true;
	}
	return false;
}
