#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "../structures/heap_monitor.h"
#include "../structures/ds_structure_types.h"

using namespace std;
using namespace structures;
class Date
{
public:
	Date(short dd, short mm, year_t yyyy);
	Date();
	~Date();
	short get_day() const;
	short get_month() const;
	year_t get_year() const;
	void print();
	std::string print_s();
	void next_day();
	bool operator==(const Date& other);
	bool operator!=(const Date& other);
	bool operator>(const Date& other);
	bool operator>=(const Date& other);
	bool operator<(const Date& other);
	bool operator<=(const Date& other);
	void save(ofstream& outfile);
	void load(ifstream& infile);
private:
	int leap_year();

	short day_;
	short month_;
	year_t year_;
};

