#include "../structures/heap_monitor.h"
#include "Spolocnost.h"



Spolocnost::Spolocnost(short den, short mesiac, year_t rok)
{
	datum_ = Date(den,mesiac,rok);
	park_ = Vozovy_park();
	sklad_ = Sklad();
}


Spolocnost::~Spolocnost()
{
}

void Spolocnost::vypis_datum()
{
	datum_.print();	
}

Date Spolocnost::get_datum()
{
	return datum_;
}

void Spolocnost::pridaj_auto()
{
	string ec = "";
	float kapacita = 0;
	char odpoved = 'y';
	Vozidlo* v = nullptr;

	cout << "Zadajte Evidencne cislo: ";
	cin >> ec;
	
	while(park_.je_vozidlo(ec))
	{
		cout << "Toto evidencne cislo uz existuje\n";
		cout << "Zadajte unikatne evidencne cislo: ";
		cin >> ec;
	}	
	cout << "Zadajte kapacitu: ";

	while (!(cin >> kapacita)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	cout << "Chcete pouzit dnesny datum ako datum pridania(y/n)?";
	
	if (can_continue())
	{
		v = new Vozidlo(ec, kapacita, datum_, false, 0);
	}
	else
	{			
		v = new Vozidlo(ec, kapacita, datum_formular(), false, 0);
	}		
	park_.pridaj_vozidlo(v);
	cout << "Vozidlo pridane." << endl;
}

void Spolocnost::vypis_vozidlo()
{
	park_.vypis_zoznam();
}

void Spolocnost::save()
{
	string cesta;
	cout << "Zadajte nazov suboru: ";
	cin >> cesta;
	
	ofstream outfile(cesta);
	if (park_.save(outfile)) 
	{
		cout << "\nVozidla sa podarilo ulozit.\n";		
	}
	else
	{
		cout << "\nVozidla sa nepodarilo ulozit.\n";
	}
	
	if (sklad_.save(outfile))
	{
		cout << "\nDodavatelov sa podarilo ulozit.\n";
	}
	else
	{
		cout << "\nDodavatelov sa nepodarilo ulozit.\n";
	}

	outfile.close();
}

void Spolocnost::load()
{
	cout << "Stratite vsetok neulozeny obsah, pokracovat (y/n)?";
	if (can_continue())
	{
		park_.vymaz_auta();
		sklad_.vymaz_dodavatelov();
		
		string cesta;

		cout << "Zadajte nazov suboru: ";
		cin >> cesta;
		
		ifstream infile(cesta);

		if (park_.load(infile))
		{
			cout << "Vozidla sa podarilo nacitat.\n";
		}
		else
		{
			cout << "\nVozidla sa nepodarilo nacitat.\n";
		}
		
		if (sklad_.load(infile))
		{
			cout << "Dodavatelov sa podarilo nacitat.\n";
		}
		else
		{
			cout << "\nDodavatelov sa nepodarilo nacitat.\n";
		}

		infile.close();
	}
	else
	{
		cout << "Zrusene.";
	}
}

bool Spolocnost::can_continue()
{
	char ch;

	while (!(cin >> ch)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	switch (ch)
	{
	case 'n':
	case 'N':
	case '0':
		return false;
	default:
	case 'y':
	case 'Y':
	case 'A':
	case 'a':
		return true;
	}
}

Adresa Spolocnost::adresa_formular()
{
	string mesto;
	string ulica;
	int noDomu;
	printf("Zadajte mesto: ");
	getline(cin, mesto, '\n');
	//cin >> mesto;

	printf("Zadajte ulicu: ");
	getline(cin, ulica, '\n');
	//cin >> ulica;

	printf("Zadajte cislo domu: ");
	while (!(cin >> noDomu)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	return Adresa(mesto, ulica, noDomu);
}

bool Spolocnost::pridaj_kamion()
{
	size_t nosnost;
	cout << "Zadajte nosnost kamionu: ";
	cin >> nosnost;
	cout << "Zadajte datum prichodu kamionu." << endl;
	Kamion* k = new Kamion(nosnost, datum_formular());
	cout << "Chcete pridat palety?(y/n)";
	if (can_continue())
	{
		cout << "Chcete nacitat subor?(y/n)";
		if (can_continue())
		{
			string cesta;			
			cout << "Zadajte cestu k suboru:";
			cin >> cesta;
			ifstream infile(cesta);
			if (sklad_.load_palety(infile, *k))
			{
				cout << "Palety sa uspesne nacitali.";
				if(sklad_.pridaj_kamion(k))
					return true;
			}
			else
			{
				cout << "Palety sa nepodarilo nacitat.";
			}
		}
		else
		{
			bool dalsia = true;
			size_t region;
			size_t vaha;
			Date datum;
			bool priorita;

			while (dalsia)
			{

				cout << "Zadajte region: ";
				cin >> region;
				cout << "Zadajte vahu: ";
				cin >> vaha;
				cout << "Je prioritna(0/1): ";
				cin >> priorita;
				Paleta* p = new Paleta(region, vaha, priorita ? datum_ : datum_formular(), priorita);
				k->pridaj_paletu(p);
				cout << "Chcete pridat paletu?(y/n)";
				if (!can_continue())
				{
					dalsia = false;
					if (sklad_.pridaj_kamion(k))
					{
						cout << "Kamion uspesne pridany." << endl;
						return true;
					}
					else
					{
						cout << "Kamion sa nepodarilo pridat." << endl;
					}
				}
			}
		}
	}
	delete k;
	return false;
}
			


Date Spolocnost::datum_formular() 
{
	short den, mesiac;
	year_t rrrr;
	bool hotovo = false;

	printf("Zadajte den: ");
	while (!(cin >> den)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	printf("Zadajte mesiac: ");
	while (!(cin >> mesiac)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	printf("Zadajte rok: ");
	while (!(cin >> rrrr)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	
	return Date(den, mesiac, rrrr);
}

void Spolocnost::pridaj_dodavatel()
{
	cin.get();
	while (true)
	{
		string pom;
		cout << "Zadajte obchodny nazov dodavatela:";
		getline(cin, pom);		

		if (!sklad_.je_dodavatel(pom))
		{
			Dodavatel* d = new Dodavatel(pom, adresa_formular());
			if (sklad_.pridaj_dodavatel(d))
			{
				cout << "Dodavatel pridany." << endl;
				break;
			}
		}
		else
		{
			cout << "Dany dodavatel uz existuje." << endl;
		}
	}
}

void Spolocnost::vypis_dodavatelov()
{
	sklad_.vypis_dodavatelov();
}










