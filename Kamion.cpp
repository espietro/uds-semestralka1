#include "Kamion.h"
#include "../structures/heap_monitor.h"


Kamion::Kamion()
{
}

Kamion::Kamion(size_t nosnost, Date datum):
	nosnost_(nosnost),
	obsadenost_(0),
	datum_(datum)
{
}


Kamion::~Kamion()
{
	vymaz_palety();
}

bool Kamion::operator==(const Kamion & other) const
{
	if (*this == other)
	{
		return true;
	}
	return false;
}

Date Kamion::get_datum()
{
	return datum_;
}

void Kamion::vymaz_palety()
{
	if (!palety_.isEmpty())
	{
		for (size_t i = 0; i < palety_.size(); i++)
		{			
			Paleta* pom = palety_.peek();
			palety_.pop();
			delete pom;			
		}
	}
}

bool Kamion::pridaj_paletu(Paleta * paleta)
{
	if (nosnost_ >= obsadenost_+paleta->get_vaha())
	{
		palety_.push(paleta);
		obsadenost_ += paleta->get_vaha();
		cout << "Paleta nacitana" << endl;
		return true;
	}
	delete paleta;
	cout << "Kamion je uz plny."<< endl;
	return false;
}

size_t Kamion::size() {
	return palety_.size();
}




