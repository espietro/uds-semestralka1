#pragma once
#include <string>
#include <iostream>
#include "../structures/heap_monitor.h"
#include "Date.h"
#include "Vozovy_park.h"
#include "Sklad.h"


class Spolocnost
{
public:
	Spolocnost(short den, short mesiac, year_t rok);
	~Spolocnost();
	inline void dalsi_den() { datum_.next_day(); };
	void vypis_datum();
	Date get_datum();
	void pridaj_auto();
	void vypis_vozidlo();
	Date datum_formular();
	void pridaj_dodavatel();
	void vypis_dodavatelov();
	Adresa adresa_formular();
	bool pridaj_kamion();

	void save();
	void load();
	bool can_continue();
	
	/*bool pridaj_vozidlo(Vozidlo* voz);
	Vozidlo zmaz_vozidlo(string ec);
	Vozidlo* najdi_vozidlo(string ec);*/

private:
	Vozovy_park park_;
	Sklad sklad_;
	Date datum_; //aktualny datum 
	//LinkedList<Dodavatel*> dodavatelia_;
};

