#include "Dodavatel.h"
#include "../structures/heap_monitor.h"



Dodavatel::Dodavatel()
{
}

Dodavatel::Dodavatel(string nazov, Adresa adresa):
	nazov_(nazov),
	adresa_(adresa)
{
}


Dodavatel::~Dodavatel()
{
}

bool Dodavatel::operator==(Dodavatel & other) const
{
	if (other.get_adresa().vypis() == adresa_.vypis() && other.get_nazov() == nazov_)
	{
		return true;
	}
	return false;
}

string Dodavatel::get_nazov() const
{
	return nazov_;
}

Adresa Dodavatel::get_adresa() const
{
	return adresa_;
}

string Dodavatel::vypis()
{
	string pom;
	pom = "Nazov: " + this->get_nazov() + ", sidlo: " + get_adresa().vypis();
	return pom;
}

bool Dodavatel::save(ofstream & outfile)
{
	if (outfile.is_open())
	{
		outfile << nazov_ << "| ";
		adresa_.save(outfile);
		return true;
	}
	return false;
}

bool Dodavatel::load(ifstream & infile)
{
	if (infile.is_open())
	{
		getline(infile, nazov_,'|');
		//infile >> nazov_;
		adresa_.load(infile);
		return true;
	}
	return false;
}


