#include "../structures/heap_monitor.h"
#include "Date.h"

Date::Date(short dd, short mm, year_t yy)
{
	day_ = dd;
	month_ = mm;
	year_ = yy;
}

Date::Date()
{
	day_ = 1;
	month_ = 1;
	year_ = 1900;
}

Date::~Date()
{
}

// Accessor functions

short Date::get_day() const
{
	return day_;
}

short Date::get_month() const
{
	return month_;
}

year_t Date::get_year() const
{
	return year_;
}

// Other functions

/**
	Vypise datum na obrazovku.
*/
void Date::print()
{
	std::cout << +day_ << '/'
		<< +month_ << '/'
		<< year_;
}

std::string Date::print_s()
{
	return std::to_string(day_) + "/" + std::to_string(month_) + "/" + std::to_string(year_);
}

/**
	Posunie tento datum o jeden den.
*/
void Date::next_day()
{
	short days_in_month;
	day_++;
	switch (month_)
	{
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12: days_in_month = 31;
		break;
	case 4:
	case 6:
	case 9:
	case 11: days_in_month = 30;
		break;
	case 2: if (leap_year())
		days_in_month = 29;
			else days_in_month = 28;
			break;
	}
	if (day_ > days_in_month)
	{
		day_ = 1;
		month_++;
		if (month_ > 12)
		{
			month_ = 1;
			year_++;
		}
	}
}

/**
	return - true ak su datumy rovnake 
*/
bool Date::operator==(const Date& other)
{
	if (other.year_ == year_ && other.month_ == month_ && other.day_ == day_)
	{
		return true;
	}
	return false;
}

/**
	return - true ak datumy nie su rovnake
*/
bool Date::operator!=(const Date & other)
{
	if (!(*this == other))
	{
		return true;
	}
	return false;
}

/**
	return - true ak je tento datum vylucne vacsi ako other
*/
bool Date::operator>(const Date & other)
{
	if (year_ > other.get_year())
	{
		return true;
	}
	else if (year_ == other.get_year() && month_ > other.get_month())
	{
		return true;
	}
	else if (year_ == other.get_year() && month_ == other.get_month() && day_ > other.get_day())
	{
		return true;
	}
	return false;
}

/**
	return - true ak je tento datum vacsi alebo rovny ako other
*/
bool Date::operator>=(const Date & other)
{
	if (*this > other || *this == other)
	{
		return true;
	}
	return false;
}

/**
	return - true ak je tento datum mensi ako 
*/
bool Date::operator<(const Date & other)
{
	if (!(*this > other) && !(*this == other))
	{
		return true;
	}
	return false;
}

/**
	return - true ak je tento datum mensi alebo rovny datumu other
*/
bool Date::operator<=(const Date & other)
{
	if (*this < other || *this == other)
	{
		return true;
	}
	return false;
}

void Date::save(ofstream & outfile)
{
	if (outfile.is_open())
	{
		outfile << day_ << " " << month_ << " " << year_ << " ";
	}
	else
	{
		cout << "Nepodarilo sa ulozit datum" << endl;
	}
}

void Date::load(ifstream & infile)
{
	if (infile.is_open())
	{
		infile >> day_;
		infile >> month_;
		infile >> year_;
	}
	else
	{
		cout << "Nepodarilo sa nacitat datum" << endl;
	}
}

// Private function

int Date::leap_year()
{
	if (year_ % 400 == 0)
		return 1;
	else if (year_ % 100 == 0)
		return 0;
	else if (year_ % 4 == 0)
		return 1;
	else return 0;
}
