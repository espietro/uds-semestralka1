#include "Sklad.h"
#include "../structures/heap_monitor.h"


Sklad::Sklad()
{
}


Sklad::~Sklad()
{
	vymaz_dodavatelov();
	vymaz_kamiony();
}

bool Sklad::load(ifstream & infile)
{
	string pom;
	infile >> pom;
	if (pom == "dodavatelia")
	{
		int poc;
		infile >> poc;
		for (int i = 0; i < poc; i++)
		{
			Dodavatel* dod = new Dodavatel();
			dod->load(infile);
			dodavatelia_.add(dod);
		}
		return true;
	}
	return false;
}

bool Sklad::save(ofstream & outfile)
{
	if (outfile.is_open())
	{
		outfile << "dodavatelia " << dodavatelia_.size() << " ";
		for each (Dodavatel* item in dodavatelia_)
		{
			item->save(outfile);
		}
		return true;
	}
	return false;
}

bool Sklad::load_palety(ifstream & infile, Kamion& kam)
{
	if (infile.is_open())
	{
		string pom;
		size_t pocet;
		infile >> pom;
		infile >> pocet;
		if (pom == "palety")
		{
			for (size_t i = 0; i < pocet; i++)
			{
				Paleta* p = new Paleta();
				p->load(infile);
				kam.pridaj_paletu(p);
			}
			return true;
		}		
	}	
	return false;
}

bool Sklad::pridaj_dodavatel(Dodavatel* dodavatel)
{
	if (!dodavatelia_.isEmpty())	//ak zoznam dodavatelov nie je prazdny tak
	{
		for (size_t i = 0; i < dodavatelia_.size(); i++)	//tak prejdeme cez existujucich dodavatelov
		{
			if (dodavatel->get_nazov() < dodavatelia_[i]->get_nazov())	//ak existuje dodavatel ktoreho nazov je podla abecedy neskor ako nazov noveho dodavatela
			{
				dodavatelia_.insert(dodavatel, i);	//tak noveho dodavatela vlozime lexikograficky podla abecedy pred uz existujuceho dod
				return true;
			}
		}
		dodavatelia_.add(dodavatel);	//tak ho len vlozime na koniec zoznamu
		return true;
	}
	else	//ak zoznam dodavatelov je prazdny
	{
		dodavatelia_.add(dodavatel);	//tak len pridame na koniec zoradenia
		return true;
	}

	return false;

}

bool Sklad::je_dodavatel(string nazov)
{
	if (!dodavatelia_.isEmpty())	//najskor zistime ci dany dodavatel uz nie je pridany v zozname
	{
		for each (Dodavatel* dod in dodavatelia_)
		{
			if (dod->get_nazov() == nazov) {
				return true;
			}
		}
	}
	return false;
}



void Sklad::vymaz_dodavatelov()
{
	if (!dodavatelia_.isEmpty())
	{
		size_t pocet = dodavatelia_.size();
		for (size_t i = 0; i < pocet; i++)
		{
			Dodavatel* pom = dodavatelia_[0];
			dodavatelia_.removeAt(0);
			delete pom;
		}
	}
}

void Sklad::vypis_dodavatelov()
{
	if (dodavatelia_.isEmpty())
	{
		cout << "Zoznam je prazdny." << endl;
	}
	else
	{
		for each (Dodavatel* item in dodavatelia_)
		{
			cout << item->vypis() << endl;
		}
	}
}

bool Sklad::pridaj_kamion(Kamion* kamion)
{
	if (ohlasene_kamiony_.isEmpty())
	{
		ohlasene_kamiony_.add(kamion);
		return true;
	}
	else
	{
		for (size_t i = 0; i < ohlasene_kamiony_.size(); i++)
		{
			if (kamion->get_datum() >= ohlasene_kamiony_[i]->get_datum())
			{
				ohlasene_kamiony_.insert(kamion, i);
				return true;
			}
		}
		ohlasene_kamiony_.add(kamion);
		return true;
	}
	return false;
}

void Sklad::vymaz_kamiony()
{
	for (size_t i = ohlasene_kamiony_.size(); i > 0; i--)
	{
		delete ohlasene_kamiony_[i - 1];
		ohlasene_kamiony_.removeAt(i - 1);
	}

}