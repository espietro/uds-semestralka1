#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include "../structures/heap_monitor.h"
#include "../structures/stack/explicit_stack.h"
#include "Paleta.h"
#include "Date.h"
#include "../structures/array/array.h"

using namespace std;
using namespace structures;
class Vozidlo
{
public:
	Vozidlo();
	Vozidlo(string ev_cis, float nosnost, Date datum_zarade, bool na_vyradenie, int opotrebenie);
	~Vozidlo();
	string get_ec();
	float get_nosnost();
	int get_opotrebenie();
	bool je_na_vyradenie();
	Date get_datum_zarad();
	string vypis();
	void vymaz_palety();
	void pridaj_paletu(Paleta* paleta);
	void save(ofstream& outfile);
	void load(ifstream& infile);
	
private:
	string ec_;	//evidencne cislo
	float nosnost_;	//nosnost vozidla
	byte opotrebenie_;	//<0,100> 0-minimalne opotrebenie
	bool na_vyradenie_;
	Date datum_zarad_;	//datum zaradenia auta do evidencie
	ExplicitStack<Paleta*> palety_;
};

