#include <iostream>
#include <ctime>
#include "../structures/array/array.h"
#include "Date.h"
#include "../structures/heap_monitor.h"
#include "Source.h"
#include "Spolocnost.h"

int main() {
	initHeapMonitor();
	using namespace structures;
	using namespace std;
	
	bool koniec = false;
	int vyber;
	
	// current date/time based on current system
	time_t now = time(0);
	tm ltm;
	localtime_s(&ltm,&now);
	/*cout << "Year" << 1900 + ltm.tm_year << endl;
	cout << "Month: " << 1 + ltm.tm_mon << endl;
	cout << "Day: " << ltm.tm_mday << endl;*/

	Spolocnost* s = new Spolocnost(ltm.tm_mday, 1 + ltm.tm_mon, 1900 + ltm.tm_year);
	
	/*Paleta* p = new Paleta(2, 30, Date(), true);
	Kamion* k = new Kamion(500, Date());
	if (k->pridaj_paletu(p))
	{
		cout << "sfas";
	}
	delete k;*/
	
	while (!koniec)
	{
		menu();
		while (!(cin >> vyber)) {
			cout << "Bad value!" << endl;
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		switch (vyber)
		{
		case 1:	s->vypis_datum(); break;
		case 2: s->dalsi_den(); break;
		case 3: s->pridaj_auto(); break;
		case 4: s->vypis_vozidlo(); break;
		case 5: s->save(); break;
		case 6: s->load(); break;
		case 7:	s->pridaj_dodavatel(); break;
		case 8:	s->vypis_dodavatelov(); break;
		case 9: s->pridaj_kamion(); break;
		default: koniec = true;
			break;
		}

	}
	delete s;
	return 0;
}

void menu() {
	using namespace std;	
	cout << endl << "=====menu======" << endl;
	cout << "0 - koniec\t\t" << "7 - pridat dodavatela" << endl;
	cout << "1 - dnesny datum\t" << "8 - vypis dodavatelov" << endl;
	cout << "2 - dalsi den\t\t" << "9 - ohlas kamion" << endl;
	cout << "3 - pridat vozidlo\t\t" << endl;
	cout << "4 - vypis vozidla\t\t" << endl;

	cout << endl << "\t\t5 - ulozit spolocnost\t\t" << endl;
	cout << "\t\t6 - nacitat spolocnost\t\t" << endl;
	
}