#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "../structures/heap_monitor.h"
#include "..\structures\list\array_list.h"
#include "../structures/queue/explicit_queue.h"
#include "Vozidlo.h"
using namespace structures;
class Vozovy_park
{
public:
	Vozovy_park();
	~Vozovy_park();
	bool pridaj_vozidlo(Vozidlo* vozidlo);
	void vypis_zoznam();

	Vozidlo odstran_vozidlo(string ec);
	bool je_vozidlo(string ec);
	void vymaz_auta();
	bool load(ifstream & infile);
	bool save(ofstream & file);
private:
	ArrayList<Vozidlo*> vozidla_;
	ExplicitQueue<Vozidlo*> na_vyradenie_;
};

